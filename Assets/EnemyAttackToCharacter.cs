using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackToCharacter : MonoBehaviour
{
    [SerializeField] private Enemy enemy;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Character characterTarget = collision.GetComponent<Character>();
        if(characterTarget)
        {
            if(characterTarget.isControl)
            {
                enemy.isAttackCharacter = true;
                enemy.isMoveToCharacter = false;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Character characterTarget = collision.GetComponent<Character>();
        if (characterTarget)
        {
            if (characterTarget.isControl)
            {
                enemy.isAttackCharacter = false;
            }
        }
    }
}
