using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int HpCurrent;
    public int HpMax;

    public GameObject TextPoup;
    public GameObject HpBar;
    private Vector3 ScacleHpBar;

    #region DamageTaked
    [SerializeField] AnimationClip Hurt;
    bool isHurt;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            ShowPoup(collision);
            isHurt = true;
            animator.Play(Hurt.name);
        }
    }
    public void ShowPoup(Collider2D collision)
    {
        DamgeToEnemy DamgeSender = collision.gameObject.GetComponentInParent<DamgeToEnemy>();
        if (DamgeSender)
        {
            PoupDame poupDame = TextPoup.GetComponent<PoupDame>();
            if (poupDame)
            { 
                poupDame.SetTextPoup(DamgeSender.DamgeInflict(0).ToString());
                Debug.Log(DamgeSender.DamgeInflict(0).ToString());
                SetHp(-DamgeSender.DamgeInflict(0));
                GameObject temp = Instantiate(TextPoup, this.transform.position, Quaternion.identity);
            }
        }
    }
    public void EndHurt()
    {
        isHurt = false;
    }
    void SetHp(int value)
    {
        HpCurrent += value;
        HpBar.transform.localScale = new Vector3(ScacleHpBar.x * HpCurrent / HpMax, ScacleHpBar.y, ScacleHpBar.x);
        if (HpCurrent <= 0) Destroy(gameObject);
    }
    #endregion


    #region Attack and Move
    [SerializeField] private Animator animator;
    [SerializeField] Character TargetAttack;
    [SerializeField] Vector3 StartScale;

    [SerializeField]private float RangeToMove;
    [SerializeField]private float RangeToAttack;
    public bool isAttackCharacter;
    public bool isMoveToCharacter;


    private void Start()
    {
        StartScale = this.transform.localScale;
        ScacleHpBar = this.HpBar.transform.localScale;
    }
    private void Update()
    {
        SetTarget();
        EnemyAction();
    }


    protected virtual void EnemyAction()
    {
        if (isHurt) return;
        if (isAttackCharacter)
        {
            int random = Random.Range(1, 3);
            animator.SetInteger("IndexAction", random);
        }
        else if (isMoveToCharacter)
        {
            if (TargetAttack)
                if (TargetAttack.transform.position.x < transform.position.x)
                {
                    this.transform.localScale = new Vector3(-StartScale.x, StartScale.y, StartScale.y);
                }
                else if (TargetAttack.transform.position.x > transform.position.x)
                {
                    this.transform.localScale = new Vector3(StartScale.x, StartScale.y, StartScale.y);
                }
            animator.SetInteger("IndexAction", 3);
        }
        if (!isMoveToCharacter && !isAttackCharacter)
        {
            animator.SetInteger("IndexAction", 0);
        }
    }

    public void OnAnimatorMove()
    {
        this.transform.position += animator.deltaPosition;
    }

    void SetTarget()
    {
        TargetAttack = FindTargetAttack();
        DistanceToMove();
        DistanceToAttack();
    }
    Character FindTargetAttack()
    {
        foreach(var character in CharacterController.Instance.characterOnScene )
        {
            if(character.isControl)
                return character;
        }
        return null;
    }
    void DistanceToMove()
    {
        if (!TargetAttack) return;
        float distance = Vector2.Distance(this.transform.position,TargetAttack.transform.position);
        if (distance <= RangeToMove) isMoveToCharacter = true;
        else isMoveToCharacter = false;
    }
    void DistanceToAttack()
    {
        if (!TargetAttack) return;
        float distance = Vector2.Distance(this.transform.position, TargetAttack.transform.position);
        if (distance <= RangeToAttack)
        {
            isAttackCharacter = true;
            isMoveToCharacter = false;
        }
        else isAttackCharacter = false;
    }
    #endregion

}
