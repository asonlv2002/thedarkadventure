using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
public class PlayerControler : MonoBehaviour
{
    [SerializeField] float JumpForce, Speed;

    Rigidbody2D rb2D;
    PhotonView PV;

    private void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        PV = GetComponent<PhotonView>();
    }
    private void Start()
    {
        if(!PV.IsMine)
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb2D);
        }
    }
    private void Update()
    {
        if (!PV.IsMine) return;
        Move();
        Jump();
    }
    void Move()
    {
        this.transform.position += Vector3.right*Input.GetAxisRaw("Horizontal") * Speed*Time.deltaTime;
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb2D.AddForce(Vector3.up * JumpForce, ForceMode2D.Impulse);
        }
    }
}
