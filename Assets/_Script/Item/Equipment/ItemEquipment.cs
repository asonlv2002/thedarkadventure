using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New ItemEquipment", menuName = "Item/Create Item Equipment")]
public class ItemEquipment : Item
{
    public bool isEquip;
    public TypeEquipment typeEquipment;

    public EquipmentInInventory EquipmentPrefab;


    public EffectBuff effectBuff;
    public EffectPassive effectPassive;
    public EffectAtive effectAtive;
}

public enum TypeEquipment
{
    Weapon,
    Head,
    Arm,
    Leg,
    Body,
    Foot,
}

