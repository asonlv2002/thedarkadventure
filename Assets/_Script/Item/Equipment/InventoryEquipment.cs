using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryEquipment : ItemInventory<ItemEquipment>
{
    [SerializeField] EquipmentInInventory equipmentPrefab;
    protected override void LoadPanel(PanelConfig panel)
    {
        var typePanel = panel.typePanel;
        var contentPanel = panel.contentPanel;
        Debug.Log("OPEN: " + typePanel + " Panel");
        switch (typePanel)
        {
            case TypePanel.All:
                break;
            case TypePanel.Weapon:
                WeaponPanel(contentPanel);
                break;
            default:
                Debug.LogError("KHONG TON TAI TYPEPANEL");
                return;
        }
    }

    void WeaponPanel(Transform contentPanel)
    {
        foreach (var item in _items)
        {
            CreateItemInPanel(item, contentPanel);
        }

    }

    protected override void CreateItemInPanel(ItemEquipment item, Transform content)
    {
        EquipmentInInventory equipmentItem = Instantiate(equipmentPrefab, content);

        equipmentItem.SetItem(item);
        equipmentItem.gameObject.SetActive(true);
    }

}
