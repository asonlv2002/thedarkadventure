using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ItemInInventory<T> : MonoBehaviour
{
    [SerializeField] protected T item;

    [Header("Item UI")]
    [SerializeField] protected Image icon;
    [SerializeField] protected Button onClickItem;

    public virtual void SetItem(T _item)
    {
        item = _item;
        SetItemUI(item);
    }

    public virtual T GetItem()
    {
        return item;
    }

    protected abstract void SetItemUI(T item);
    protected abstract void ShowInfoItem();

    protected virtual void OnDestroy()
    {
        //onClickItem.onClick.RemoveAllListeners();
    }
}

