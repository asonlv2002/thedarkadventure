using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public abstract class PanelInventory<T> : MonoBehaviour
{
    public List<T> items;
    [SerializeField] protected List<PanelConfig> panels;
    protected PanelConfig currentPanel;

    protected virtual void Start()
    {
        SetUpButton();
    }
    protected virtual void SetUpButton()
    {
        foreach (var panel in panels)
        {
            panel.buttonOpen.onClick.AddListener(() => ChangePanel(panel.typePanel));
        }
    }

    protected virtual void ChangePanel(TypePanel type)
    {
 
        foreach (var panel in panels)
        {
            if(panel.typePanel == type)
            {
                currentPanel = panel;
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }
        ClearPanel();
    }

    protected virtual void ClearPanel()
    {
        Transform contant = currentPanel.contentPanel;
        foreach(Transform child in contant)
        {
            Destroy(child.gameObject);
        }
    }

    protected virtual void LoadPanel(TypePanel typePanel)
    { }
}


