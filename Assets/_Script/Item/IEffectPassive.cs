using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEffectPassive
{
    public void AcviveEffect();
}

[System.Serializable]
public class EffectPassive
{
    public bool HadEffectPassive => effects.Count > 0;
    public List<EffectPassiveConfig> effects;
}

[System.Serializable]
public class EffectPassiveConfig
{
    public string nameEffect;
    [TextArea]
    public string description;
}
