using UnityEngine;

[System.Serializable]
public class ItemConfig
{
    [Space(15)]
    public string id;
    public TypeItem typeItem;
    public string name;
    public Sprite icon;
    [TextArea]
    public string description;

    public ItemBusiness itemBusiness;
}

public enum TypeItem
{
    None,
    UseOnly,
    Equipment,
    Metarial
}

public class ItemBusiness
{
    public float priceBuy;
    public float priceSell;

    public bool CanBuy => priceBuy > 0;
    public bool CanSell => priceSell > 0;
}