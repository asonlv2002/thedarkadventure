using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentInInventory : ItemInInventory<ItemEquipment>
{
    protected override void SetItemUI(ItemEquipment item)
    {
        icon.sprite = item.itemConfig.icon;
        onClickItem.onClick.AddListener(() => ShowInfoItem());
    }

    protected override void ShowInfoItem()
    {
        Debug.Log("SHOWINFO: " + item.itemConfig.name);
    }
}
