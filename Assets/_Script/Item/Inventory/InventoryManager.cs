using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InventoryManager : Singleton<InventoryManager>
{
    public List<InventoryPanelConfig> goupInventorys;
    public InventoryEquipment inventoryEquipment;
}

public class InventoryPanelConfig
{
    public TypeItem typeInventory;
    public GameObject inventory;
    public Button onClickInventory;
}