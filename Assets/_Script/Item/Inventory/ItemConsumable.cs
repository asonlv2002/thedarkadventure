using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New ItemConsumable",menuName ="Item/ Create Item Consumable")]
public class ItemConsumable : Item
{
    public EffectAtive effectAtive;
}
