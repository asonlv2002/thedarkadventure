using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemOnRoad<T> : MonoBehaviour
{
    [SerializeField] protected T item;
    [SerializeField] protected SpriteRenderer icon;

    protected const int INDEX_CHARACTER_LAYER = 6;

    protected abstract void OnCollisionEnter2D(Collision2D collision);

    protected abstract bool CheckTargetPick(GameObject target);

    protected abstract void AddItemToInVentory();

    protected virtual void PickItem(GameObject target)
    {
        if (CheckTargetPick(target) == true)
            AddItemToInVentory();
    }

    protected virtual void DestroyItem()
    {
        Destroy(this.gameObject);
    }
}
