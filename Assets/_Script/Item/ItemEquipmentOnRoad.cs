using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEquipmentOnRoad : ItemOnRoad<ItemEquipment>
{
    private void OnEnable()
    {
        icon.sprite = item.itemConfig.icon;
    }

    protected override bool CheckTargetPick(GameObject target)
    {
        int layerTarget = target.layer;

        return layerTarget == INDEX_CHARACTER_LAYER;
    }

    protected override void AddItemToInVentory()
    {
        InventoryManager.Instance.inventoryEquipment.AddItem(item);
        DestroyItem();
    }

    protected override void OnCollisionEnter2D(Collision2D other)
    {
        GameObject target = other.gameObject;
        PickItem(target);

    }

    protected override void PickItem(GameObject target)
    {
        base.PickItem(target);
    }

    protected override void DestroyItem()
    {
        base.DestroyItem();
    }
}
