using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public interface IEffectAtive
{
    public void ActiveEffect();
}

[System.Serializable]
public class EffectAtive
{
    public bool HadEffectAtive => effects.Count > 0;
    public List<EffectAtiveConfig> effects;
}

[System.Serializable]
public class EffectAtiveConfig
{
    public string nameEffect;
    [TextArea]
    public string description;
}


