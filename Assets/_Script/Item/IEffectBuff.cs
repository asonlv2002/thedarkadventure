using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEffectBuff
{
    public List<EffectBuffConfig> GetStatValueImpact();
}

[System.Serializable]
public class EffectBuff
{
    public bool HadEffectBuff => effects.Count > 0;
    public List<EffectBuffConfig> effects;
}

[System.Serializable]
public class EffectBuffConfig
{
    public StatHard stat;
    public float value;

    [TextArea]
    public string description;
}

public enum StatHard
{
    MaxHp,
    MaxMp,
    Attack,
    SpeedAttack,
    Speed

}
