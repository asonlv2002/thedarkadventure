using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public abstract class ItemInventory<T> : MonoBehaviour
{
    [Header("===ITEMS DATA===")]
    [SerializeField] protected List<T> _items;

    [Header("===PANEL===")]
    [SerializeField] protected List<PanelConfig> panels;
    protected PanelConfig currentPanel;

    protected virtual void Start()
    {
        SetUpButton();
    }

    #region Items Control
    public List<T> GetItems()
    {
        return _items;
    }

    public T GetItemByIndex(int index)
    {
        return _items[index];
    }

    public void AddItem(T item)
    {
        _items.Add(item);
    }
    #endregion


    #region PanelControl

    protected virtual void SetUpButton()
    {
        foreach (var panel in panels)
        {
            panel.buttonOpen.onClick.AddListener(() => ChangePanel(panel.typePanel));
        }
    }

    protected virtual void ChangePanel(TypePanel type)
    {

        foreach (var panel in panels)
        {
            if (panel.typePanel == type)
            {
                currentPanel = panel;
                panel.SetActive(true);
            }
            else
            {
                panel.SetActive(false);
            }
        }
        ClearPanel(currentPanel);
        LoadPanel(currentPanel);
    }

    protected virtual void ClearPanel(PanelConfig panel)
    {
        Transform contant = panel.contentPanel;
        foreach (Transform child in contant)
        {
            Destroy(child.gameObject);
        }
    }

    protected abstract void LoadPanel(PanelConfig panel);

    protected abstract void CreateItemInPanel(T item, Transform content);
    #endregion;
}
[System.Serializable]
public class PanelConfig
{
    public TypePanel typePanel;
    public Transform contentPanel;
    public Button buttonOpen;
    public void SetActive(bool value)
    {
        contentPanel.gameObject.SetActive(value);
    }
}
public enum TypePanel
{
    All,
    Weapon,
    Potion
}