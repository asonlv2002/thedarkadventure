using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControl : MonoBehaviour
{
    [SerializeField]
    private bool isDestroy;
    [SerializeField]
    private bool isSpawn;

    [SerializeField]
    private GameObject nextObject;

    [SerializeField]
    private Vector3 nextPost;

    public void EndAnimation()
    {
        if (isSpawn)
        {
            GameObject temp = Instantiate(nextObject, this.transform.position + nextPost, Quaternion.identity);
            temp.SetActive(true);
        }
        if (isDestroy) Destroy(gameObject);
    }
}
