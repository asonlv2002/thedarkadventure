using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : Singleton<PopupManager>
{
    [SerializeField] Canvas _canvas;

    [SerializeField] List<Popup> _popupList;

    private void Start()
    {
        SetupCanvas();
    }

    public void OpenPopup(TypePopup type)
    {
        Popup popupOpen = _popupList.Find(x => x.TypePopup == type);
        popupOpen.SetActive(true);
    }

    void SetupCanvas()
    {
        _canvas.worldCamera = Camera.main;
    }

}

public enum TypePopup
{
    Inventory,
    Equipment
}
