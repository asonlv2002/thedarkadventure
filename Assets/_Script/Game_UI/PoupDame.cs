using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PoupDame : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Animator animator;
    public void SetTextPoup(string value)
    {
        text.SetText(value);
    }
    public void ShowPoup()
    {
        this.gameObject.SetActive(true);
    }
    public void OnAnimatorMove()
    {
        this.transform.position += animator.deltaPosition;
    }
}
