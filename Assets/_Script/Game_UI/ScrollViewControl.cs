using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScrollViewControl : MonoBehaviour
{
    public List<Transform> ListPage;

    public TextMeshProUGUI NamePage;

    public Button btnNext;

    public Button btnPrev;

    int indexPage;
    public int IndexPage // indexControl;
    {
        get { return indexPage; }
        set
        {
            indexPage += value;
            if(indexPage >= ListPage.Count || indexPage <=0)
            {
                indexPage = 0;
                btnNext.gameObject.SetActive(true);
                btnPrev.gameObject.SetActive(false);
                return;

            }
            if(indexPage == ListPage.Count-1)
            {
                btnNext.gameObject.SetActive(false);
                btnPrev.gameObject.SetActive(true);
                return;
            }
            btnPrev.gameObject.SetActive(true);
            btnNext.gameObject.SetActive(true);
        }
    }

    private void OnEnable()
    {
        IndexPage = 0;
        ShowPage(IndexPage);
        btnNext.onClick.AddListener(() => { ClickOnNext(); });
        btnPrev.onClick.AddListener(() => { ClickOnPrev(); });
    }

    public void ShowPage(int index)
    {
        for (int i = 0; i < ListPage.Count; i++)
        {
            if (i == index)
            {
                ListPage[i].gameObject.SetActive(true);
                NamePage.text = ListPage[i].gameObject.name;
            }
            else
            {
                ListPage[i].gameObject.SetActive(false);
            }
        }
    }
    private void ClickOnNext()
    {
        IndexPage = 1;
        ShowPage(IndexPage);
    }

    private void ClickOnPrev()
    {
        IndexPage = -1;
        ShowPage(IndexPage);
    }
}
