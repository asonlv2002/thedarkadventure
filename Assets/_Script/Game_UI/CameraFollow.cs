using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private float speedfollow;

    [SerializeField]
    private Transform targetFollow;

    [SerializeField]
    private Vector3 Offset;

    public Transform TargetFollow
    {
        set { targetFollow = value; }
    }

    private void Update()
    {
        if (!targetFollow) return;
        Vector3 newPos = this.targetFollow.position + Offset;
        transform.position = Vector3.Slerp(transform.position, newPos, speedfollow*Time.deltaTime);
    }
}
