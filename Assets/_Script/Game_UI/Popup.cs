using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Popup : MonoBehaviour
{
    [SerializeField] TypePopup _typePopup;
    [SerializeField] Button _buttonClose;
    public TypePopup TypePopup => _typePopup;
    private void Start()
    {
        SetButton();
    }

    void ClosePopup()
    {
        SetActive(false);
    }

    void SetButton()
    {
        _buttonClose.onClick.AddListener(() => ClosePopup());
    }

    public void SetActive(bool value)
    {
        this.gameObject.SetActive(value);
    }
}

