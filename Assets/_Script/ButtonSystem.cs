using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSystem : MonoBehaviour
{
    [SerializeField] List<ButtonShowPopup> _listButtonShowPopup;

    private void Start()
    {
        SetuoButton();
    }

    void SetuoButton()
    {
        foreach(var button in _listButtonShowPopup)
        {
            button.SetButtonEvent();
        }
    }
}

[System.Serializable]
public class ButtonOption
{
    [SerializeField] Button _button;
    public Button Button => _button;

}

[System.Serializable]
public class ButtonShowPopup
{
    [SerializeField] TypePopup _typePopup;
    [SerializeField] Button _openPopup;

    public void SetButtonEvent()
    {
        _openPopup.onClick.AddListener(() => { PopupManager.Instance.OpenPopup(_typePopup); });
    }
}
