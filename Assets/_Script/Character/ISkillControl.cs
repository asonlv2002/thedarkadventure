using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface ISkillControl
{
    public void StartSkill();
    public void EndSkill();

    public void Attack1();
    public void Attack2();
    public void Attack3(); 
    public void Attack4();  
    public void Attack5();
    public void Ultimate();

}
