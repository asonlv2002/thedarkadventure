using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Genaster : Character
{
    public void OnCollisionExit2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = false;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = true;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
    public void OnCollisionStay2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = true;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
}
