using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSkill : MonoBehaviour, ISkillControl
{
    public Animator Animator;
    public string NameCharacter;
    [SerializeField] protected Transform PosSpawn;

    [SerializeField] protected DamgeToEnemy Attack_1;
    [SerializeField] protected DamgeToEnemy Attack_2;
    [SerializeField] protected DamgeToEnemy Attack_3;
    [SerializeField] protected DamgeToEnemy Attack_4;
    [SerializeField] protected DamgeToEnemy Attack_5;
    [SerializeField] protected DamgeToEnemy Attack_Ultimate;

    DamgeToEnemy SkillSpawn;

    public void StartSkill()
    {
        CharacterController.Instance.Attack.SetIsAttack(true);
        CharacterController.Instance.Moverment.SetIsAttack(true);
    }
    public void EndSkill()
    {
        CharacterController.Instance.Attack.SetIsAttack(false);
        CharacterController.Instance.Moverment.SetIsAttack(false);

        Animator.Play(NameCharacter + "_Idle");
    }
    public virtual void Attack1()
    {
        SkillSpawn = Instantiate(Attack_1, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }

    public virtual void Attack2()
    {
        SkillSpawn = Instantiate(Attack_2, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }

    public virtual void Attack3()
    {
        SkillSpawn = Instantiate(Attack_3, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }

    public virtual void Attack4()
    {
        SkillSpawn = Instantiate(Attack_4, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }

    public virtual void Attack5()
    {
        SkillSpawn = Instantiate(Attack_5, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }
    

    public virtual void Ultimate()
    {
        SkillSpawn = Instantiate(Attack_Ultimate, PosSpawn.position, Quaternion.identity);
        SetDirection(SkillSpawn.gameObject);
        SkillSpawn.gameObject.SetActive(true);
    }
    
    void SetDirection(GameObject gameObject)
    {
        if(this.transform.localScale.x <0)
        {
            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * -1,
                                                          gameObject.transform.localScale.y,
                                                          gameObject.transform.localScale.z);
        }
    }
}
