using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShubaltaSkil : CharacterSkill
{
    public override void Attack1()
    {
        Attack_1.ATK_Character = 100;
        base.Attack1();
        
    }
    public override void Attack2()
    {
        Attack_2.ATK_Character = 100;
        base.Attack2();
        
    }
    public override void Attack3()
    {
        Attack_3.ATK_Character = 100;
        base.Attack3();
       
    }
    public override void Attack4()
    {
        Attack_4.ATK_Character = 100;
        base.Attack4();
        
    }
    public override void Attack5()
    {
        Attack_5.ATK_Character = 100;
        base.Attack5();
        
    }
    public override void Ultimate()
    {
        Attack_Ultimate.ATK_Character = 100;
        base.Ultimate();
        
    }
}
