using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAction : MonoBehaviour
{
    public string NameCharater;

    protected const string _Idle = "_Idle";
    protected const string _MoveFoward = "_MoveFoward";
    protected const string _MoveBack = "_MoveBack";
    protected const string _JumpUp = "_JumpUp";
    protected const string _JumpDow = "_JumpDown";

    protected const string _Attack1 = "_Attack1";
    protected const string _Attack2 = "_Attack2";
    protected const string _Attack3 = "_Attack3";
    protected const string _Attack4 = "_Attack4";
    protected const string _Attack5 = "_Attack5";
    protected const string _Ultimate = "_Ultimate";
}
