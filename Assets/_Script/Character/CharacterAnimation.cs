using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour
{
    public string NameCharacter;
    public Animator Animator;
    public List<string> listNameAnimation;
    public void PlayAnimation(string name)
    {
        if(listNameAnimation.IndexOf(name) == -1)
        {
            Debug.Log("Animation Error");
            return;
        }
        Animator.Play(name);
    }
}
