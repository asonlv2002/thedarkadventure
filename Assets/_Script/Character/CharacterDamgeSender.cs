using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDamgeSender : MonoBehaviour
{
    [SerializeField] private Character character;

    public int DamgeInflict(int DEF)
    {
        return character.characterStatInfo.ATK - DEF;
    }
}
