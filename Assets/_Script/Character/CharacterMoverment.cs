using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoverment : MonoBehaviour
{
    public string NameCharacter;

    protected const string _Idle = "_Idle";
    protected const string _MoveFoward = "_MoveFoward";
    protected const string _MoveBack = "_MoveBack";
    protected const string _JumpUp = "_JumpUp";
    protected const string _JumpDown = "_JumpDown";

    public Transform CharTarget;
    public float Speed;

    public Rigidbody2D CharRb2D;
    public float JumpForce;
    public bool isGround;

    private bool isFlip;
    private bool isIdle;
    public bool IsIdle
    {
        get => isIdle;
        set => isIdle = value;
    }
    public bool isAttack;
    private float moveStatus;

    [SerializeField] float fallMultiplier = 2.5f;
    [SerializeField] float lowMultiplier = 2f;

    private void Update()
    {
        if (isAttack) return;
        Flip();
        Movement();
        Jump();
        if (CharTarget)
        ChoseAnimation(moveStatus * CharTarget.localScale.x, isAttack, isGround);
    }

    public void Flip()
    {
        isFlip = Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl) ;

        if(isFlip)
        {
            this.CharTarget.localScale = new Vector3(this.CharTarget.localScale.x * -1, this.CharTarget.localScale.y, this.CharTarget.localScale.z);
        }
    }
    public void Movement()
    {
        if (CharTarget == null) return;
        moveStatus = Input.GetAxisRaw("Horizontal");
        this.CharTarget.position += Vector3.right * moveStatus * JumpForce * Time.deltaTime;
    }
    public void Jump()
    {
        if(CharRb2D == null) return;
        if (Input.GetButton("Jump") && isGround)
        {
            if (!CharRb2D) return;
            this.CharRb2D.velocity = Vector2.up * JumpForce;
        }
        JumpSmooth();
    }
    
    public void JumpSmooth()
    {
        // this https://www.youtube.com/watch?v=7KiK0Aqtmzc video will help you
        if (CharRb2D.velocity.y < 0)
        {
            CharRb2D.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;

        }
        else if(CharRb2D.velocity.y > 0 && !Input.GetButton("Jump"))
        {
            CharRb2D.velocity += Vector2.up * Physics2D.gravity.y * (lowMultiplier - 1) * Time.deltaTime;
        }
        
    }
    public void SetIsGroud(bool value)
    {
        this.isGround = value;
    }
    public void SetIsAttack(bool value)
    {
        isAttack = value;
    }

    public void ChoseAnimation(float input, bool isAttack, bool isGround )
    {
        if (NameCharacter == "") return;
        if (isAttack) return;

        if (input > 0)
        {
            isIdle = false;
            CharacterController.Instance.Animation.PlayAnimation(NameCharacter+_MoveFoward);
            //CharacterManager.Instance.SyncMoving(_MoveFoward);
            return;
        }
        else if (input < 0)
        {
            isIdle = false;
            CharacterController.Instance.Animation.PlayAnimation(NameCharacter + _MoveBack);
            //CharacterManager.Instance.SyncMoving(_MoveBack);
            return;
        }
        else if (input == 0 && isGround)
        {
            isIdle = true;
            CharacterController.Instance.Animation.PlayAnimation(NameCharacter + _Idle);
            //CharacterManager.Instance.SyncMoving(_Idle);
            return;
        }
        else if (input == 0 && !isGround)
        {
            isIdle = false;
            if (CharRb2D.velocity.y >= 0) CharacterController.Instance.Animation.PlayAnimation(NameCharacter +_JumpUp);
            if(CharRb2D.velocity.y <0 ) CharacterController.Instance.Animation.PlayAnimation(NameCharacter+_JumpDown);
            return;
        }
    }
}
