using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatus : MonoBehaviour
{
    public GameObject TextPoup;
    public Character Character;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 9)
        {
            ShowPoup(collision);
        }
    }
    public void ShowPoup(Collider2D collision)
    {
        DamgeToCharacter DamgeSender = collision.gameObject.GetComponentInParent<DamgeToCharacter>();
        if (DamgeSender)
        {
            PoupDame poupDame = TextPoup.GetComponent<PoupDame>();
            if (poupDame)
            {
                poupDame.SetTextPoup(DamgeSender.DamgeInflict(0).ToString());
                //Debug.Log(DamgeSender.DamgeInflict(0).ToString());
                GameObject temp = Instantiate(TextPoup, this.transform.position, Quaternion.identity);
            }
        }
    }
}
