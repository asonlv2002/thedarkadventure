using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterSystem",menuName = "CharacterObject/CharacterSystem", order = 1)]
[SerializeField]
public class CharacterInforObject : ScriptableObject
{
    #region UI Character
    public string NameCharacter;
    public Animator animator;
    public Rigidbody2D rb;
    public BoxCollider2D collider;
    public Transform Transform;
    #endregion

    #region Parameter Character

    public float Speed;
    public float JumbForce;

    public int HPMax;
    public int HPCurrent;
    public int MPMax;
    public int MPCurrent;
    public int DEF;
    public int ATK;
    #endregion
}
