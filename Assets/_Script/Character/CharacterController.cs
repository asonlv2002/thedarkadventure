using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : Singleton<CharacterController>
{
    #region Singleton Linked
    public string NameCharacter;
    public CharacterAnimation Animation;
    public CharacterAttack Attack;
    public CharacterAttributes Attributes;
    public CharacterMoverment Moverment;

    public CameraFollow CameraFollow;

    #endregion

    public Character CurrentCharacter;

    [SerializeField]
    public List<Character> characterOnScene;

    private void Start()
    {
        SpawnCharacter();
    }

    private void Update()
    {
        SwitchCharacter();
    }
    #region Switch Character
    private void SwitchCharacter()
    {
        ChoseCharacterByMouse();
        ChoseCharacterByKey(KeyCode.RightShift);
    }
    private void ChoseCharacterByMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 OnPosClick = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            RaycastHit2D hit = Physics2D.Raycast(OnPosClick, Vector2.zero);

            if (hit.collider)
            {
                Debug.Log("Yes");
                Character choseCharacter = hit.collider.GetComponent<Character>();
                if (choseCharacter)
                {
                    ChoseControllCharacter(choseCharacter.nameCharacter);
                }
            }
            SyncAnimation();
        }
    }
    private void ChoseCharacterByKey(KeyCode key)
    {
        if(Input.GetKeyDown(key))
        {
            int indexCurrent = characterOnScene.IndexOf(CurrentCharacter);
            if(indexCurrent == characterOnScene.Count-1)
            {
                indexCurrent = 0;
                
            }
            else
            {
                indexCurrent++;
            }
            ChoseControllCharacter(characterOnScene[indexCurrent].nameCharacter);
            SyncAnimation();
            return;
        }
    }

    private void SyncAnimation()
    {
        foreach (Character character in characterOnScene)
        {
            if(!character.isControl)
            {
                string name = character.nameCharacter;
                character.characterSystem.Animator.Play(name + "_Idle");
            }
        }
    }
    #endregion
    private void SpawnCharacter()
    {
        float space = 0;
        if (CharacterManager.Instance.characterList.Count == 0) return;
        foreach (Character character in CharacterManager.Instance.characterList)
        {
            space-= 2f;
            Character temp = Instantiate(character, this.transform.position+Vector3.right*space, Quaternion.identity);
            characterOnScene.Add(temp);
            temp.transform.SetParent(this.transform);
        }
        ChoseControllCharacter(characterOnScene[0].nameCharacter);
    }
    public void ChoseControllCharacter(string NameCharacter)
    {
        Debug.Log("Change");

        CurrentCharacter = FindCharacter(NameCharacter);

        this.NameCharacter = NameCharacter;
        Moverment.CharTarget = CurrentCharacter.gameObject.transform;
        Moverment.CharRb2D = CurrentCharacter.characterSystem.Rb;
        Moverment.JumpForce = CurrentCharacter.characterStatInfo.JumbForce;

        Animation.Animator = CurrentCharacter.characterSystem.Animator;
        Animation.listNameAnimation = CurrentCharacter.listNameAnimation;
        Animation.NameCharacter = CurrentCharacter.nameCharacter;

        Attack.NameCharacter = CurrentCharacter.nameCharacter;
        Moverment.NameCharacter = CurrentCharacter.nameCharacter;

        Attributes.SetHpCharacterCurrent(CurrentCharacter.characterStatInfo.HPMax, CurrentCharacter.characterStatInfo.HPCurrent);
        Attributes.SetMpCharacterCurrent(CurrentCharacter.characterStatInfo.MPMax, CurrentCharacter.characterStatInfo.MPCurrent);

        CameraFollow.TargetFollow = CurrentCharacter.transform;
    }

    public Character FindCharacter(string name)
    {
        Character current = null;
        for (int i = 0; i < characterOnScene.Count; i++)
        {
            if (characterOnScene[i].nameCharacter == name)
            {

                current = characterOnScene[i];
                characterOnScene[i].isControl = true;
            }
        }
        for (int i = 0; i < characterOnScene.Count; i++)
        {
            if (characterOnScene[i].nameCharacter != name)
            {
                characterOnScene[i].isControl = false;
            }
        }
        return current;
    }

}
