using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttributes : MonoBehaviour
{
    [SerializeField]
    private UI_BarStatus barStatus;

    private CharacterStatInfo StatInfo;

    public void SetCharatcerStat(CharacterStatInfo value)
    {
        StatInfo = value;
    }
    public CharacterStatInfo GetCharacterSat()
    {
        return StatInfo;
    }
    

    public void SetHpCharacterCurrent(int MaxHP, int currentHP)
    {
        this.barStatus.heathPoint.maxValue = MaxHP; 
        this.barStatus.heathPoint.value = currentHP;
        this.barStatus.heathCurrent = currentHP;
    }
    public void SetMpCharacterCurrent(int MaxMp, int currentMp)
    {
        this.barStatus.manaPoint.maxValue = MaxMp;
        this.barStatus.manaPoint.value = currentMp;
        this.barStatus.manaCurrent = currentMp;

    }
}
