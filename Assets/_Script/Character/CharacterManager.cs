using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : Singleton<CharacterManager>
{
    public List<Character> characterList;

    public Character FindCharacter(string name)
    {
        Character current = null;
        for (int i = 0; i < characterList.Count; i++)
        {
            if (characterList[i].nameCharacter == name)
            {

                current = characterList[i];
                characterList[i].isControl = true;
            }
        }
        for (int i = 0; i < characterList.Count; i++)
        {
            if (characterList[i].nameCharacter != name)
            {
                characterList[i].isControl = false;
            }
        }
        return current;
    }

    public void SyncMoving(string NameAction)
    {
        for(int i = 0; i < characterList.Count; i++)
        {
            if (!characterList[i].isControl)
            {
                string name = characterList[i].nameCharacter;
                characterList[i].characterSystem.Animator.Play(name + NameAction);
            }
        }
    }
}
