using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Character : MonoBehaviour
{
    public bool isControl;
    public bool isGround;
    public CharacterSystem characterSystem;
    public CharacterStatInfo characterStatInfo;
    public string nameCharacter;

    public List<string> listNameAnimation;

    public void OnAnimatorMove()
    {
        if (!isControl) return;
        this.transform.position += characterSystem.Animator.deltaPosition;
        this.transform.rotation = Quaternion.Euler(
            characterSystem.Animator.deltaRotation.x,
            characterSystem.Animator.deltaRotation.y,
            characterSystem.Animator.deltaRotation.z);
    }

    public virtual void OnCollisionExit2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = false;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = true;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
    public virtual void OnCollisionStay2D(Collision2D collision)
    {
        if (!isControl) return;
        isGround = true;
        CharacterController.Instance.Moverment.SetIsGroud(isGround);
    }
}

[System.Serializable]
public class CharacterSystem
{
   #region UI Character
    public string NameCharacter;
    public Animator Animator;
    public Rigidbody2D Rb;
    public BoxCollider2D collider;
    public Transform Transform;
    #endregion
}

[System.Serializable]
public class CharacterStatInfo
{
    public float Speed;
    public float JumbForce;

    public int HPMax;
    public int HPCurrent;
    public int MPMax;
    public int MPCurrent;
    public int DEF;
    public int ATK;
}