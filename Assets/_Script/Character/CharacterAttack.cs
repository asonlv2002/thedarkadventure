using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAttack : MonoBehaviour
{
    public string NameCharacter;

    public float SpeedAttack; //for Attack Normal;

    public bool T_GetKeyDown; // => Attack_1 ~ Attack Normal;
    public bool Q_GetKeyDown; // => Attack_2;
    public bool W_GetKeyDown; // => Attack_3;
    public bool E_GetKeyDown; // => Attack_4;
    public bool R_GetKeyDown; // => Attack_5;

    public bool Shift_GetKeyDown; // => Can spawnSkill;
    public float CountDown_ActiveSkill;
    public float CountDown_ActiveSkillmax = 0.5f;

    [SerializeField]
    private string CodeAttack;

    protected const string _Attack1 = "_Attack1";
    protected const string _Attack2 = "_Attack2";
    protected const string _Attack3 = "_Attack3";
    protected const string _Attack4 = "_Attack4";
    protected const string _Attack5 = "_Attack5";
    protected const string _Ultimate = "_Ultimate";

    public bool isAttack;
    public bool checkSpawnSkill;

    private void Update()
    {
        if (isAttack) return;

        if (CountDown_ActiveSkill > 0)
        {
            CountDown_ActiveSkill -= Time.deltaTime;
            Attack();
            return;
        }
        Shift_GetKeyDown = Input.GetKeyDown(KeyCode.LeftShift);
        CodeAttack = "";
        checkSpawnSkill = false;
        if (Shift_GetKeyDown)
        {
            CodeAttack += "Shift_";
            checkSpawnSkill = true;
            CountDown_ActiveSkill = CountDown_ActiveSkillmax;
        }
        Attack();
        

    }

    public void Attack()
    {
        if (!CharacterController.Instance.Moverment.IsIdle) return;
        T_GetKeyDown = Input.GetKeyDown(KeyCode.T);
        if (T_GetKeyDown)
        {
            CodeAttack += "T";
            if(CodeAttack == "Shift_T")
            {
                Debug.Log("Active");
                ChoseAnimation(NameCharacter + _Ultimate);
                CountDown_ActiveSkill = 0;
                CodeAttack = "";
                return;
            }
            ChoseAnimation(NameCharacter + _Attack1);
            CountDown_ActiveSkill = 0;
            CodeAttack = "";
            return;
        }

        Q_GetKeyDown = Input.GetKeyDown(KeyCode.Q);
        if (Q_GetKeyDown)
        {
            CodeAttack += "Q";
            ChoseAnimation(NameCharacter + _Attack2);
            CountDown_ActiveSkill = 0;
            CodeAttack = "";
            return;
        }

        W_GetKeyDown = Input.GetKeyDown(KeyCode.W);
        if (W_GetKeyDown)
        {
            CodeAttack += "W";
            ChoseAnimation(NameCharacter + _Attack3);
            CountDown_ActiveSkill = 0;
            CodeAttack = "";
            return;
        }

        E_GetKeyDown = Input.GetKeyDown(KeyCode.E);
        if (E_GetKeyDown)
        {
            CodeAttack += "E";
            ChoseAnimation(NameCharacter + _Attack4);
            CountDown_ActiveSkill = 0;
            CodeAttack = "";
            return;
        }

        R_GetKeyDown = Input.GetKeyDown(KeyCode.R);
        if (R_GetKeyDown)
        {
            CodeAttack += "R";
            ChoseAnimation(NameCharacter + _Attack5);
            CountDown_ActiveSkill = 0;
            CodeAttack = "";
            return;
        }
    }

    public void ChoseAnimation(string Name)
    {
        CharacterController.Instance.Animation.PlayAnimation(Name);
    }

    public void SetIsAttack(bool value)
    {
        isAttack = value;
    }
}
