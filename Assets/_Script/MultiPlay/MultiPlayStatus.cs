using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class MultiPlayStatus : MonoBehaviour
{
    string multiplayStatus;
    [SerializeField] TextMeshProUGUI m_text;

    // Update is called once per frame
    void Update()
    {
        this.multiplayStatus = PhotonNetwork.NetworkClientState.ToString();
        this.m_text.SetText(multiplayStatus);
    }
}
