using System.Collections;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultiPlayLogin : MonoBehaviourPunCallbacks
{
    public TMP_InputField inputUserName;

    private void Start()
    {
        inputUserName.text = "Sonlv2002";
    }

    public virtual void Login()
    {
        string name = inputUserName.text;
        Debug.Log(transform.name + "Login: "+ name);

        PhotonNetwork.LocalPlayer.NickName = name;
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
        PhotonNetwork.JoinLobby();
    }
}
