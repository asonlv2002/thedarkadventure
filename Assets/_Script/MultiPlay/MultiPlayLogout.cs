using Photon.Pun;
using TMPro;
using UnityEngine;

public class MultiPlayLogout : MonoBehaviour
{
    public virtual void Logout()
    {
        Debug.Log("Logout");
        PhotonNetwork.Disconnect();
    }
}
