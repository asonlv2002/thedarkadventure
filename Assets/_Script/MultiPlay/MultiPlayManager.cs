using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.IO;

public class MultiPlayManager : MonoBehaviourPunCallbacks
{
    public static MultiPlayManager Instance;

    private void Awake()
    {
        if(Instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if(scene.buildIndex ==1)
        { 
            PhotonNetwork.Instantiate(Path.Combine("PlayerPrefab","PlayerManager"), Vector3.zero, Quaternion.identity);
            //Ghi ?�ng ??a ch? trong th? m?c Resource - Path.Combine();
        }
        
    }
}
