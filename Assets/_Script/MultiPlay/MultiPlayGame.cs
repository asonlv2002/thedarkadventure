using System.Collections;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class MultiPlayGame : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI networkStatus;
    [SerializeField] TextMeshProUGUI namePlayerUI;
    string NamePlayer;

    private void Start()
    {
        NamePlayer = PhotonNetwork.NickName.ToString();
        namePlayerUI.text = NamePlayer;
    }
    private void Update()
    {
        this.networkStatus.text ="Status:"+ PhotonNetwork.NetworkClientState.ToString();
    }

    public void Leave()
    {
        string name = NamePlayer;
        Debug.Log(transform.name +"LeaveRoom: "+ name);
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("MultiPlay_Room");
    }
}
