using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using TMPro;

public class MultiPlayRoom : MonoBehaviourPunCallbacks
{
    public static MultiPlayRoom Instance;
    public TMP_InputField input;
    public Animator test;
    public Transform Content;
    public UIRoomProfile roomProfilePrefab;
    public List<RoomInfo> updatedRooms;
    public List<RoomProfile> RoomProfiles = new List<RoomProfile>();


    public void Hiii()
    {
        test.Play("test");
    }
    private void Awake()
    {
        MultiPlayRoom.Instance = this;
    }
    void Start()
    {
        test.StopPlayback();
        input.text = "Room1";
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void Create()
    {
        string name = input.text;
        Debug.Log(transform.name + "Create: " + name);
        PhotonNetwork.CreateRoom(name);
    }

    public void Join()
    {
        string name = input.text;
        Debug.Log(transform.name + "Join: " + name);
        PhotonNetwork.JoinRoom(name);
        this.ClearRoomUI();
    }

    public void Leave()
    {
        string name = input.text;
        Debug.Log(transform.name + "Leave:" + name);
        PhotonNetwork.LeaveRoom();
    }
    public void StartGame()
    {
        if(PhotonNetwork.NetworkClientState != ClientState.Joined)
        {
            return;
        }
        string name = input.text;
        Debug.Log(transform.name + "StartGame:" + name);
        PhotonNetwork.LoadLevel("GamePlay");
    }
    public override void OnCreatedRoom()
    {
        Debug.Log("OnCreatedRoom");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        StartGame();
    }
    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnCreateRoomFailed" + message);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log(transform.name + ": OnRoomListUpdate");
        this.updatedRooms = roomList;

        foreach (RoomInfo roomInfo in roomList)
        {
            if (roomInfo.RemovedFromList) this.RoomRemove(roomInfo);
            else this.RoomAdd(roomInfo);
        }

        this.LoadRoomUI();
    }

    protected virtual void RoomAdd(RoomInfo room)
    {
        RoomProfile roomProfile;

        roomProfile = this.RoomByName(room.Name);
        if (roomProfile != null) return;

        roomProfile = new RoomProfile
        {
            RoomName = room.Name
        };
        this.RoomProfiles.Add(roomProfile);
    }

    protected virtual void RoomRemove( RoomInfo room)
    {
        RoomProfile roomProfile = this.RoomByName(room.Name);
        if (roomProfile == null) return;
        this.RoomProfiles.Remove(roomProfile);
    }
    protected virtual RoomProfile RoomByName(string name)
    {
        foreach(var roomProfile in this.RoomProfiles)
        {
            if (roomProfile.RoomName == name) return roomProfile;
        }
        return null;
    }

    #region ListRoomUI
    protected virtual void LoadRoomUI()
    {
        this.ClearRoomUI();
        foreach(var room in this.RoomProfiles)
        {
            UIRoomProfile uiRoomProfile = Instantiate(this.roomProfilePrefab);
            uiRoomProfile.SetRoomProfile(room);
            uiRoomProfile.transform.SetParent(this.Content);
        }
    }
    protected virtual void ClearRoomUI()
    {
        foreach (Transform child in this.Content)
        {
            Destroy(child.gameObject);
        }
    }
    #endregion
}
