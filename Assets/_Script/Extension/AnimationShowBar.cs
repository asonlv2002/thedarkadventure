using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimationShowBar : MonoBehaviour
{
    [SerializeField] Button _btnOpen;
    [SerializeField] Button _btnClose;
    [SerializeField] Transform _objectBar;
    [SerializeField] Transform _endPos;
    [SerializeField] Transform _startPos;
    [SerializeField] [Range(10,20)]
    float _speed;
    private void Start()
    {
        _btnOpen.onClick.AddListener(this.Open);
        _btnClose.onClick.AddListener(this.Close);

    }
    public void Open()
    {
        _objectBar.position = _startPos.position;
        _objectBar.gameObject.SetActive(true);

        _btnClose.gameObject.SetActive(true);
        _btnOpen.gameObject.SetActive(false);

        InvokeRepeating(nameof(this.ActveOpen), 0f, Time.fixedDeltaTime);
    }
    public void Close()
    {
        
        InvokeRepeating(nameof(this.ActiveClose), 0f, Time.fixedDeltaTime);

    }

    void ActveOpen()
    {
        
        if((Vector2)_objectBar.position != (Vector2)_endPos.position)
        {
            _objectBar.position = Vector2.MoveTowards(_objectBar.position, _endPos.position, _speed);
        }
        else
        {
            Debug.Log("Open");
            CancelInvoke(nameof(this.ActveOpen));
        }
    }
    void ActiveClose()
    {
        if ((Vector2)_objectBar.position != (Vector2)_startPos.position)
        {
            _objectBar.position = Vector2.MoveTowards(_objectBar.position, _startPos.position, _speed);
        }
        else
        {
            Debug.Log("Close");
            CancelInvoke(nameof(this.ActiveClose));
            _btnClose.gameObject.SetActive(false);
            _btnOpen.gameObject.SetActive(true);
            _objectBar.gameObject.SetActive(false);

        }
    }
}
