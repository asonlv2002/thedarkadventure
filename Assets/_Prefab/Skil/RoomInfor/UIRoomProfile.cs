using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class UIRoomProfile : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI roomName;
    [SerializeField] RoomProfile roomProfile;

    public void SetRoomProfile(RoomProfile roomProfile)
    {
        this.roomProfile = roomProfile;
        this.roomName.text = roomProfile.RoomName;
    }

    public void OnClick()
    {
        Debug.Log("Click Room: "+ roomName.text);
        MultiPlayRoom.Instance.input.text = roomName.text;
    }
}
