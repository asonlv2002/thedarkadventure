using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveToCharacter : MonoBehaviour
{
    [SerializeField] private Enemy enemy;
    private void OnTriggerStay2D(Collider2D collision)
    {
        Character characterTarget = collision.GetComponent<Character>();
        if(characterTarget)
        {
            if(characterTarget.isControl)
            {
                //enemy.TargetAttack = characterTarget.transform;
                enemy.isMoveToCharacter = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Character characterTarget = collision.GetComponent<Character>();
        if (characterTarget)
        {
            if (characterTarget.isControl)
            {
                //enemy.TargetAttack = null;
                enemy.isMoveToCharacter = false;
            }
        }
    }
}
